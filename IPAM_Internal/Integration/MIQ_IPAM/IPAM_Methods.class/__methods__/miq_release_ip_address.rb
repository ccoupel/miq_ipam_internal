###################################
#
# EVM Automate Method: miq_release_ip_address
#
# Notes: EVM Automate method to release IP Address information from EVM Automate Model
#


begin

  #######
  #  We need to build in a rescue release of IP if a provision request failed to complete but DID reserve an IP
  #######
  # Get Provisioning Object: This will be nil if manually provisioned outside of CloudForms
  prov = $evm.root["miq_provision"]
  log(:info, "#{@method}  Inspect prov object: <#{prov.inspect}>") 

  # Get current VM object, this will be nil if a provision request failed during processing
  vm = $evm.root['vm']
#  raise "#{@method} VM or Provision Object not found and cannot release IP" if vm.nil? && prov.nil?

  vm_name=$evm.object["vm_name_to_release"]
  
  ipam_db_name = $evm.object["ipam_db_name"]

  ## We have an IPAM Database name, so we can go on
  log(:info, " IPAM DB Name: #{ipam_db_name}")

  ## We get the IPAM Database path from the IPAM configuration and database name
  IPAM_preamble = $evm.object['path_to_ipams']

  ## We get the IPAM Database path from the IPAM configuration and database name
  ipam_db_path = "#{IPAM_preamble}#{ipam_db_name}"
  log(:info, "Release IP from IPAM DB: ipam_db_path: #{ipam_db_path}")
  search_path = "#{ipam_db_path}/*"
  log(:info, " Release IP from IPAM DB: #{ipam_db_path}")

  ## Find an instance that matches the VM's IP Address
  instance_hash = instance_find(search_path)
  raise "No instances found in <#{search_path.inspect}>" if instance_hash.empty?

  ## Look for IP Address candidate that validates hostname
  ip_candidate = instance_hash.find { |k, v| v['vm_name'] == vm_name }
  if ip_candidate.nil?
    log(:warn, " No instance matches the VM '#{vm_name}', so nothing to be done.")
    exit MIQ_OK
  end

  ## Assign first element in array to the instance name
  class_instance = ip_candidate.first

  ## Assign last element to new_hash, so that we can update its attributes
  new_hash = ip_candidate.last

  location = "#{ipam_db_path}/#{class_instance}"
  log(:info, " Found instance: <#{location}> with Values: <#{new_hash.inspect}>")

  ## Set the inuse attribute to false and the release date to now
  new_hash['inuse'] = 'false'
  new_hash['date_released'] = Time.now.strftime('%a, %b %d, %Y at %H:%M:%S %p')
  new_hash['date_acquired'] = nil
  new_hash['vm_name'] = nil
  
  ## Update instance and display name
  if instance_update(location, new_hash)
    set_displayname(location, nil)
  else
    raise "#{@method} Failed to update instance:<#{location}>"
  end

  #
  # Exit method
  #
  log(:info, "===== EVM Automate Method: <#{@method}> Ended")
  exit MIQ_OK

    #
    # Set Ruby rescue behavior
    #
rescue => err
  log(:error, "<#{@method}>: [#{err}]\n#{err.backtrace.join("\n")}")
  exit MIQ_ABORT
end
###################################
# End of EVM Automate Method: miq_release_ip_address
###################################
