###################################
#
# EVM Automate Method: miq_acquire_ip_address
#
# Notes: EVM Automate method to acquire IP Address information from EVM Automate Model
#
###################################


##############
# Gets an IP address
# @param search_path [String] the path in which we look for IP addresses
# @param ipam_db_name [String] the name of the IPAM Database we are looking into
# @return [Hash] the hash corresponding to the entry in the IPAM Database
##############
def get_free_ip_address(search_path, ipam_db_name)
  # Retrieve all the instances in the search path and raise an exception if none is returned
  instance_hash = instance_find("#{search_path}")
  raise "No instances found in <#{search_path.inspect}>" if instance_hash.empty?

  # Remove hash elements where inuse = true|TRUE and raise an exception if the hash is empty
  instance_hash.delete_if { |k, v| v['inuse'] =~ /^true$/i }
  raise "No IP Addresses are free" if instance_hash.empty?

  return instance_hash.first
end


##############
# Reserves an IP address
# @param ip_candidate [String] the IP address candidate to reserve
# @param location [String] the location of the IP address candidate
# @param reservation_token [String]
##############
def reserve_ip_address(ip_candidate, location, reservation_token)
  log(:info, " Attempt to reserve IP:<#{location}> with <#{ip_candidate.inspect}>")

  ip_candidate['inuse'] = 'true'
  ##  Now we'll use the request ID as a unique token to set on the IPAM row.  Later in the code, we'll check to ensure
  ##  our unique token still has the IPAM row reserved, if not, we'll get another IPAM row and set & check again.
  ip_candidate['reserve_token'] = reservation_token

  updated = instance_update(location, ip_candidate)
  log(:info, "#186 Reserved  #{location}: #{ip_candidate['hostname']}/#{ip_candidate['ipaddr']} with reservation token: #{ip_candidate['reserve_token']}")
  log(:info, "#187 Reserved IP address: <#{updated}> with Values:#{location} => #{ip_candidate.inspect}")
end

def update_vm_network(ip_candidate)

  # Override Customization Specification
  @prov.set_option(:sysprep_spec_override, [true, 1])
  if @prov.options.has_key?(:ws_values)
    @ws_values = @prov.options[:ws_values]
  else
    @ws_values = Hash.new
  end

  vm_name=@prov.get_option(:vm_target_name).to_s.strip
  
  if ip_candidate['hostname'].present?
    vm_name=ip_candidate['hostname'].to_s.strip
    # Use vm_name information from acquired IPAM hostname
    log(:info, "dynamic_hostname <#{dynamic_hostname}> Using IPAM value")
    @prov.set_option(:vm_target_name, vm_name)
    @prov.set_option(:linux_host_name, vm_name)
    @prov.set_option(:vm_name, vm_name)
    @prov.set_option(:vm_target_hostname, vm_name)
    @prov.set_option(:host_name, vm_name)
  end

  # Use VLAN information from acquired IPAM entry
  if ip_candidate['vlan'].present?
    # If the workflow specified to use a distributed virtual switch it would be have the value "is_dvs=true"
    # in the ws_values array which was set in CustomizeRequest method
    use_dvs = ip_candidate['is_dvs'] || false

    default_vlan = ip_candidate['vlan']
    @prov.set_vlan(default_vlan)

    log(:info, "use_dvs: #{use_dvs}")
    if boolean(use_dvs)
      @prov.set_network_adapter(nic_id, {:network => ip_candidate['vlan'], :is_dvs => true})
    else
      @prov.set_network_adapter(nic_id, {:network => ip_candidate['vlan']})
    end
  end


  # Use IP information from acquired IPAM entry
  if ip_candidate['ipaddr'].present?
    log(:info, "set_nic_settings(vlan)")
    @prov.set_nic_settings(nic_id, {:ip_addr => ip_candidate['ipaddr'], :subnet_mask => ip_candidate['submask'], :gateway => ip_candidate['gateway'], :addr_mode => ["static", "Static"]})
  end
    retrun vm_name
end

###########  Begin Processing  ###########
begin
  # Get Provisioning Object
  nic_id=$evm.object['nic_id'] || 0
  @prov = $evm.root["miq_provision"]
  @prov_type=$evm.root["vmdb_object_type"]
  @prov = $evm.root[@prov_type]
  # This entire method (AcquireIPAddress) is only used for workflows requiring the IPAM values.
  # Therefore, the following tag is the driver to let this code know which IPAM DB to call.
  # This tag MUST be created and set in the CustomizeRequest state, otherwise we raise an exception.
  ipam_db_name = $evm.object["ipam_db_name"]
  log(:info, "Using ipam_db_name <#{ipam_db_name}>")
  raise "<ipam_db_name> is nil" if ipam_db_name.nil?

  IPAM_preamble = $evm.object['path_to_ipams']

  # We get the IPAM Database path from the IPAM configuration and database name
  ipam_db_path = "#{IPAM_preamble}#{ipam_db_name}"
  log(:info, "ipam_db_path: #{ipam_db_path}")
  search_path = "#{ipam_db_path}/*"

  # We use the request ID as a unique identifier to reserve the IPAM entry
  reservation_token = "#{@prov_type}_#{@prov.id rescue nil}"


    # Call IPAM DB and get back a IP entry instance
    ip_candidate_hash = get_free_ip_address(search_path, ipam_db_name)

    # Set the IPAM instance location so we can call the DB and get just this IPAM row
    ip_candidate_path = "#{ipam_db_path}/#{ip_candidate_hash.first}"
    ip_candidate = ip_candidate_hash.last
    log(:info, "IP Candidate <#{ip_candidate_path.inspect}> : <#{ip_candidate.inspect}>")

    # Try to reserve the IP address by flipping the "inuse" flag and setting the "reserve_token" (requestion ID)
    reserve_ip_address(ip_candidate, ip_candidate_path, reservation_token)

  vm_name=update_vm_network(ip_candidate)

  
  # Set date time acquired
  ip_candidate['date_released'] = nil
  ip_candidate['date_acquired'] = Time.now.strftime('%D at %H:%M:%S %p %Z')
  ip_candidate['vm_name'] = vm_name

  # Update instance and displayname
  if instance_update(ip_candidate_path, ip_candidate)
    # Set Displayname of instance to reflect acquired IP Address
    displayname = "#{vm_name}"
    set_displayname(ip_candidate_path, displayname)
  else
    raise "Failed to update instance:<#{ip_candidate_key}>"
  end


  log(:info, "Inspecting Provision object: #{@prov.inspect}")


  #
  # Exit method
  #
  log(:info, "===== EVM Automate Method: <#{@method}> Ended")
  exit MIQ_OK

    #
    # Set Ruby rescue behavior
    #
rescue => err
  log(:error, "[#{err}]\n#{err.backtrace.join("\n")}")
  exit MIQ_ABORT
end
###################################
# End of EVM Automate Method: miq_acquire_ip_address
###################################
