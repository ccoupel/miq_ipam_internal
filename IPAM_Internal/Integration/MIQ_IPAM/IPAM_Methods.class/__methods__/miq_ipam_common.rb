###################################
#
# EVM Automate Common Method
#
# Notes: Common method for internal IPAM management
#
###################################

###################################
# Method for logging
###################################
def log(level, message)
  @method = 'miq_acquire_ip_address'
  @debug = true
  $evm.log(level, " - - #{message}") if @debug
end


##############
# Converts a string to a boolean
# @param string [String] the string to convert
# @return [Boolean] the value cast from the string
##############
def boolean(string)
  return true if string == true || string =~ (/(true|t|yes|y|1)$/i)
  return false if string == false || string.nil? || string =~ (/(false|f|no|n|0)$/i)

  # Return false if string does not match any of the above
  log(:info, "Invalid boolean string:<#{string}> detected. Returning false")
  return false
end


##############
# Finds instances based on a datastore path
# @param path [String] the path where to look for instances : MUST start with the domain name
# @return [Hash] the hash containing all the instances under the path
##############
def instance_find(path)
  result = $evm.instance_find(path)
  log(:info, "Path:<#{path}> - Properties:<#{result.inspect}>")
  return result
end

############################
# Method: instance_exists
# Notes: Returns string: true/false
############################
def instance_exists(path)
  result = $evm.instance_exists?(path)
  if result
    $evm.log('info', "Instance: <#{path}> exists. Result:<#{result.inspect}>") 
  else
    $evm.log('info', "Instance: <#{path}> does not exist. Result:<#{result.inspect}>") 
  end
  return result
end

##############
# Updates the data of a specific instance
# @param path [String] the path of the instance to update
# @param data [Hash] the data that will update the instance
# @return [Boolean] true if the update was successful, false otherwise
##############
def instance_update(path, data)
  log(:info, "Path:<#{path}> - Data:<#{data.inspect}> ")

  result = $evm.instance_update(path, data)
  if result
    log(:info, "Instance: <#{path}> updated. Result:<#{result.inspect}>")
  else
    $evm.log("error", "Instance: <#{path}> NOT updated. Result:<#{result.inspect}>")
  end
  return result
end



##############
# Sets an instance DisplayName
# @param path [String] the path of the instance we want to set the display name
# @param display_name [String] the display name we want to set
# @return [Boolean]: true if the display name has been set, false otherwise
##############
def set_displayname(path, display_name)
  result = $evm.instance_set_display_name(path, display_name)
  return result
end

