This provides IPAM functionnality to the CFME automation.

It is based on the locking mechanism described here: ** [cfme_semaphores](https://bitbucket.org/ccoupel/cfme_semaphores/src/master/) **

it requires write enable class. each class is a subnet containing an instance for every availlable IP

The schema of the class is describe as example in: ** / IPAM_Internal / Integration / MIQ_IPAM / Network_Lookup / ipam_db_10_11_11_0 **.

each instance provides:

- *vlan*: the VLAN name based on the provider vlan name
- *is_dvs*: for VCenter provider, indicate wether the VLAN is DVSor not
- *hostname*: if set, enforce the hostname, if not set, keep the standard naming mechanism
- *ipaddr*: the ip address
- *submask*: the subnet 
- *gateway*: the default gateway

the folowing attributes are set by the reservation mechanism

- *in_use*: if set to true, the corresponding ip address can not be selected as availlable. It is set to true when the ip is reserved and set to false otherwise
- *date_released*: last date when the IP was released
- *date_acquired*: last date when the IP was reserved
- *reserve_token*: unic_id help to confirm the reservation based on object type and is id
- *vm_name*: the name of the VM. is set to the hostname if it is provided or the standard vm_name

